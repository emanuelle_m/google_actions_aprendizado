'use strict';

const { dialogflow, Suggestions, Permission } = require('actions-on-google');

const functions = require('firebase-functions');

const app = dialogflow({ debug: true });

const dicesSuggestions = new Suggestions("4 faces", "6 faces", "8 faces", "10 faces", "12 faces", "20 faces");

function rollDice(diceFaceNumber) {
  return Math.floor(Math.random() * diceFaceNumber + 1);
}

app.intent('New Default Welcome', (conv) => {

  const name = conv.user.storage.userName;
  if (!name) {
    conv.ask(new Permission({
      context: 'Olá, gostaria de conhecer você melhor',
      permissions: 'NAME',
    }));
  } else {
    conv.ask(`Olá novamente, ${name}. Você gostaria de rolar os dados?`);
  }
});

app.intent('actions_intent_PERMISSION', (conv, params, permissionGranted) => {
  if (!permissionGranted) {
    conv.ask(`Ok, sem problemas, gostaria de rolar os dados?`);
    conv.ask(dicesSuggestions);
  } else {
    conv.data.userName = conv.user.name.display;
    conv.ask(`Obrigada, ${conv.data.userName}. Gostaria de rolar os dados?`);
    conv.ask(dicesSuggestions);
  }
});

app.intent('role os dados', (conv, { number }) => {

  if (number == 4 || number == 6 || number == 8 || number == 10 || number == 12 || number == 20) {

    conv.ask("A face do dado é " + rollDice(number));

  } else {
    conv.ask('O valor informado não corresponde à uma face de dado válida');
    conv.ask(dicesSuggestions);
  }
});

exports.dialogflowFirebaseFulfillment = functions.https.onRequest(app);